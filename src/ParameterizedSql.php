<?php

namespace ch\_4thewin\SqlSelectModels;



use ch\_4thewin\SqlSelectModels\Arguments\Argument;

class ParameterizedSql implements ParameterizedSqlInterface
{

    /** @var Argument[] */
    protected array $arguments;

    protected string $sql;

    /**
     * @param Argument[] $arguments
     * @param string $sql
     */
    public function __construct(array $arguments, string $sql)
    {
        $this->arguments = $arguments;
        $this->sql = $sql;
    }


    function getArguments(): array
    {
       return $this->arguments;
    }

    function toString(): string
    {
        return $this->sql;
    }
}