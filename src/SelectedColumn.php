<?php

namespace ch\_4thewin\SqlSelectModels;

class SelectedColumn
{
    /**
     * An object able to create the
     * string representation of the column
     * expression.
     * @var StringInterface
     */
    protected StringInterface $columnExpression;

    /**
     * The optional alias for the column.
     * If no name is given, the DB decides on
     * the name. It is usually the column expression string.
     * @var string|null
     */
    protected ?string $alias;

    /**
     * @param StringInterface $columnExpression
     * @param string|null $alias
     */
    public function __construct(StringInterface $columnExpression, ?string $alias = null)
    {
        $this->columnExpression = $columnExpression;
        $this->alias = $alias;
    }

    /**
     * @return StringInterface
     */
    public function getColumnExpression(): StringInterface
    {
        return $this->columnExpression;
    }

    /**
     * @return string|null
     */
    public function getAlias(): ?string
    {
        return $this->alias;
    }

    /**
     * @param string|null $alias
     */
    public function setAlias(?string $alias): void
    {
        $this->alias = $alias;
    }
}