<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;

/**
 * Used to add column orderings to a select statement.
 * @package ch\_4thewin\SqlSelectModels
 */
class Ordering
{

    /**
     * An ordering has to
     * refer to a column.
     * @var StringInterface
     */
    protected StringInterface $columnExpression;

    /**
     * If true, the ordering is ascending
     * otherwise descending.
     * @var bool
     */
    protected bool $isAscending;

    /**
     * Ordering constructor.
     * @param StringInterface $columnExpression
     * @param bool $isAscending
     */
    public function __construct(StringInterface $columnExpression, bool $isAscending = true)
    {
        $this->columnExpression = $columnExpression;
        $this->isAscending = $isAscending;
    }

    /**
     * @return StringInterface
     */
    public function getColumnExpression(): StringInterface
    {
        return $this->columnExpression;
    }

 

    /**
     * @return bool
     */
    public function isAscending(): bool
    {
        return $this->isAscending;
    }

    public function setIsAscending(bool $isAscending): self
    {
        $this->isAscending = $isAscending;
        return $this;
    }
}