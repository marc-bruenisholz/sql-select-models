<?php

namespace ch\_4thewin\SqlSelectModels;

interface StringInterface
{
    /**
     * @return string
     */
    function toString(): string;
}