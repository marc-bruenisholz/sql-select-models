<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;

/**
 * In SQL, tables are e.g. selected
 * or joined by their name but can
 * be given an alias so that they can
 * be accessed by another name within the query.
 * Aliases are important if the same
 * table is joined multiple times
 * with different conditions.
 * @package ch\_4thewin\SqlSelectModels
 */
class Table implements FromClause
{
    /**
     * The name of the table in the database.
     * @var string
     */
    protected string $name;

    /**
     * An optional alias for the table
     * for further usage in the query.
     * @var string|null
     */
    protected ?string $alias;

    /**
     * A specific primary key column name
     * can be given.
     * @var string
     */
    protected string $primaryKeyColumnName;

    /**
     * The type of the primary key column values.
     * @var string One of "integer" or "string"
     */
    protected string $primaryKeyColumnType;

    /**
     * Table constructor.
     * @param string $name
     * @param string $primaryKeyColumnName
     * @param string $primaryKeyColumnType
     * @param string|null $alias
     */
    public function __construct(string $name, string $primaryKeyColumnName, string $primaryKeyColumnType, ?string $alias = null)
    {
        $this->name = $name;
        $this->primaryKeyColumnName = $primaryKeyColumnName;
        $this->primaryKeyColumnType = $primaryKeyColumnType;
        $this->alias = $alias;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAlias(): string
    {
        return $this->alias ?? $this->name;
    }

    public function setAlias(?string $alias): self
    {
        $this->alias = $alias;
        return $this;
    }

    /**
     * @return string
     */
    public function getPrimaryKeyColumnName(): string
    {
        return $this->primaryKeyColumnName;
    }

    /**
     * @return string
     */
    public function getPrimaryKeyColumnType(): string
    {
        return $this->primaryKeyColumnType;
    }


}