<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;


/**
 * This class is abstract because there
 * are subclasses for the different types
 * of joins.
 * @package ch\_4thewin\SqlSelectModels
 */
class Join
{
    /**
     * The table owning the foreign key 
     * @var Table
     */
    protected Table $owningTable;

    /**
     * The column name containing the primary key of
     * the table to join.
     * @var string
     */
    protected string $foreignKeyColumnName;

    /**
     * The table owning the foreign key
     * @var Table
     */
    protected Table $inverseTable;


    /**
     * Indicates whether the owning table or
     * the inverse table is being joined.
     * @var bool
     */
    protected bool $isFromInverseToOwningTable;
    
    /**
     * Only rows from that fulfill
     * the condition are joined.
     * Note that you do not have to 
     * state the base join condition that
     * is needed to join the rows of the table
     * that is joined.
     * With a condition, join type INNER ist assumed,
     * otherwise LEFT.
     * @var ?ParameterizedSqlInterface
     */
    protected ?ParameterizedSqlInterface $condition;

    /**
     * Set the type explicitly (LEFT or INNER)
     * or leave it empty
     * @var string|null
     */
    protected ?string $type = null;

    /**
     * @param Table $owningTable
     * @param string $foreignKeyColumnName
     * @param Table $inverseTable
     * @param bool $isFromInverseToOwningTable
     * @param ParameterizedSqlInterface|null $condition
     * @param string|null $type
     */
    public function __construct(Table $owningTable, string $foreignKeyColumnName, Table $inverseTable, bool $isFromInverseToOwningTable, ?ParameterizedSqlInterface $condition = null, ?string $type = null)
    {
        $this->owningTable = $owningTable;
        $this->foreignKeyColumnName = $foreignKeyColumnName;
        $this->inverseTable = $inverseTable;
        $this->isFromInverseToOwningTable = $isFromInverseToOwningTable;
        $this->condition = $condition;
        $this->type = $type;
    }

    /**
     * @return Table
     */
    public function getOwningTable(): Table
    {
        return $this->owningTable;
    }

    /**
     * @return string
     */
    public function getForeignKeyColumnName(): string
    {
        return $this->foreignKeyColumnName;
    }

    /**
     * @return Table
     */
    public function getInverseTable(): Table
    {
        return $this->inverseTable;
    }

    /**
     * @return ParameterizedSqlInterface|null
     */
    public function getCondition(): ?ParameterizedSqlInterface
    {
        return $this->condition;
    }

    /**
     * @param ParameterizedSqlInterface|null $condition
     */
    public function setCondition(?ParameterizedSqlInterface $condition): void
    {
        $this->condition = $condition;
    }

    /**
     * @return bool
     */
    public function isFromInverseToOwningTable(): bool
    {
        return $this->isFromInverseToOwningTable;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }


}