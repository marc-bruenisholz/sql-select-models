<?php

namespace ch\_4thewin\SqlSelectModels;

interface FromClause
{
    function getAlias(): string;
}