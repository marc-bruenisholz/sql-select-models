<?php

namespace ch\_4thewin\SqlSelectModels\Arguments;


class FloatArgument extends Argument
{
    /**
     * @var float
     */
    protected float $floatArgument;

    /**
     * @param float $floatArgument
     */
    public function __construct(float $floatArgument)
    {
        $this->floatArgument = $floatArgument;
    }

    /**
     * @return float
     */
    public function getFloatArgument(): float
    {
        return $this->floatArgument;
    }
}