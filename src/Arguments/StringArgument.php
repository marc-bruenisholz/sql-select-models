<?php

namespace ch\_4thewin\SqlSelectModels\Arguments;


class StringArgument extends Argument
{
    /**
     * @var string
     */
    protected string $stringArgument;

    /**
     * @param string $stringArgument
     */
    public function __construct(string $stringArgument)
    {
        $this->stringArgument = $stringArgument;
    }

    /**
     * @return string
     */
    public function getStringArgument(): string
    {
        return $this->stringArgument;
    }
}