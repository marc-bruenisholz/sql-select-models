<?php

namespace ch\_4thewin\SqlSelectModels\Arguments;


class IntArgument extends Argument
{
    /**
     * @var int
     */
    protected int $intArgument;

    /**
     * @param int $intArgument
     */
    public function __construct(int $intArgument)
    {
        $this->intArgument = $intArgument;
    }

    /**
     * @return int
     */
    public function getIntArgument(): int
    {
        return $this->intArgument;
    }
}