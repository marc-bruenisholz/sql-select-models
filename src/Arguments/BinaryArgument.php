<?php

namespace ch\_4thewin\SqlSelectModels\Arguments;


class BinaryArgument extends Argument
{
    /**
     */
    protected $binaryArgument;

    /**
     * @param $binaryArgument
     */
    public function __construct($binaryArgument)
    {
        $this->binaryArgument = $binaryArgument;
    }

    /**

     */
    public function getBinaryArgument()
    {
        return $this->binaryArgument;
    }
}