<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;


/**
 * Depicts a sql select statement.
 * An instance of this class can be converted
 * to a sql query string.
 * @package ch\_4thewin\SqlSelectModels
 */
class Select implements FromClause
{
    /**
     * @var array<string, Select> Common Table Expressions (CTEs). The array key is the CTE table name, the value is a Select object.
     */
    protected array $CTEs = [];

    /**
     * @var FromClause The SQL FROM clause either refers to a table or a sub select.
     */
    protected FromClause $fromClause;

    /**
     * The columns of the select
     * statement. If none are given,
     * all columns of the $fromTable
     * are selected with a star (*).
     * @var SelectedColumn[]
     */
    protected array $selectedColumns = [];

    /**
     * The joins for
     * the select statement.
     * @var Join[]
     */
    protected array $joins = [];

    /**
     * Column orderings for
     * the select statement.
     * @var Ordering[]
     */
    protected array $orderings = [];

    /**
     * Removes duplicates when set to true
     * @var bool
     */
    protected bool $isDistinct = false;

    /** @var ParameterizedSqlInterface|null */
    protected ?ParameterizedSqlInterface $whereCondition = null;

    /**
     * @var StringInterface[]
     */
    protected array $groupByColumnExpressions = [];

    /** @var Page|null */
    protected ?Page $page = null;

    /**
     * @param FromClause $fromClause
     */
    public function __construct(FromClause $fromClause)
    {
        $this->fromClause = $fromClause;
    }


    public function selectColumn(StringInterface $columnExpression, ?string $alias = null): self
    {
        $this->selectedColumns[] = new SelectedColumn($columnExpression, $alias);
        return $this;
    }

    public function addJoin(Join $join): self
    {
        $this->joins[] = $join;
        return $this;
    }

    public function addOrdering(Ordering $ordering): self
    {
        $this->orderings[] = $ordering;
        return $this;
    }
    
    public function addCTE(string $cteName, Select $select): self
    {
        $this->CTEs[$cteName] = $select;
        return $this;
    }

    /**
     * @return SelectedColumn[]
     */
    public function getSelectedColumns(): array
    {
        return $this->selectedColumns;
    }

    /**
     * @param SelectedColumn[] $selectedColumns
     * @return $this
     */
    public function setSelectedColumns(array $selectedColumns): self
    {
        $this->selectedColumns = $selectedColumns;
        return $this;
    }

    /**
     * @return Join[]
     */
    public function getJoins(): array
    {
        return $this->joins;
    }

    /**
     * @param Join[] $joins
     * @return $this
     */
    public function setJoins(array $joins): self
    {
        $this->joins = $joins;
        return $this;
    }

    /**
     * @return bool
     */
    public function isDistinct(): bool
    {
        return $this->isDistinct;
    }

    public function setIsDistinct(bool $isDistinct): self
    {
        $this->isDistinct = $isDistinct;
        return $this;
    }

    /**
     * @return ParameterizedSqlInterface|null
     */
    public function getWhereCondition(): ?ParameterizedSqlInterface
    {
        return $this->whereCondition;
    }

    public function setWhereCondition(?ParameterizedSqlInterface $whereCondition): self
    {
        $this->whereCondition = $whereCondition;
        return $this;
    }

    /**
     * @return Ordering[]
     */
    public function getOrderings(): array
    {
        return $this->orderings;
    }

    /**
     * @param Ordering[] $orderings
     * @return $this
     */
    public function setOrderings(array $orderings): self
    {
        $this->orderings = $orderings;
        return $this;
    }

    /**
     * @return Page|null
     */
    public function getPage(): ?Page
    {
        return $this->page;
    }

    /**
     * @param Page|null $page
     * @return Select
     */
    public function setPage(?Page $page): Select
    {
        $this->page = $page;
        return $this;
    }

    /**
     * @return array<string, SELECT>
     */
    public function getAllCTEs(): array
    {
        return $this->CTEs;
    }

    public function getCTE(string $cteName): Select
    {
        return $this->CTEs[$cteName];
    }

    /**
     * @return FromClause
     */
    public function getFromClause(): FromClause
    {
        return $this->fromClause;
    }

    /**
     * @param FromClause $fromClause
     * @return Select
     */
    public function setFromClause(FromClause $fromClause): Select
    {
        $this->fromClause = $fromClause;
        return $this;
    }

    public function getAlias(): string
    {
        return $this->fromClause->getAlias();
    }

    /**
     * @return StringInterface[]
     */
    public function getGroupByColumnExpressions(): array
    {
        return $this->groupByColumnExpressions;
    }

    /**
     * @param StringInterface[] $groupByColumnExpressions
     * @return Select
     */
    public function setGroupByColumnExpressions(array $groupByColumnExpressions): Select
    {
        $this->groupByColumnExpressions = $groupByColumnExpressions;
        return $this;
    }

   

}