<?php

namespace ch\_4thewin\SqlSelectModels;

use ch\_4thewin\SqlSelectModels\Arguments\Argument;

interface ParameterizedSqlInterface extends StringInterface
{
    /**
     * @return Argument[]
     */
    function getArguments(): array;
}