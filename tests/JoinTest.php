<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;


use PHPUnit\Framework\TestCase;

class JoinTest extends TestCase
{
    public function test()
    {
        $dummyCondition = new TestParameterizedSql();
        $a = new Join(
            new Table('owningTable', 'id', 'string'),
            'foreignKey',
            new Table('tableName1', 'id', 'string','tableAlias1'),
            true,
            $dummyCondition
        );
        self::assertEquals('tableName1', $a->getInverseTable()->getName());
        self::assertEquals('tableAlias1', $a->getInverseTable()->getAlias());
        self::assertEquals($dummyCondition, $a->getCondition());
        self::assertTrue($a->isFromInverseToOwningTable());
        $dummyCondition2 = new TestParameterizedSql();

        $a->setCondition($dummyCondition2);
        self::assertEquals($dummyCondition2, $a->getCondition());
        self::assertEquals(new Table('owningTable', 'id', 'string'), $a->getOwningTable());
        self::assertEquals('foreignKey', $a->getForeignKeyColumnName());
    }

}
