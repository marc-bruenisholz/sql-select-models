<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;


use PHPUnit\Framework\TestCase;

class SelectedColumnTest extends TestCase
{
    public function test()
    {
        $dummyColumnExpression1 = new TestString();
        $dummyColumnExpression2 = new TestString();
        $a = new SelectedColumn($dummyColumnExpression1, 'columnName1');
        $b = new SelectedColumn($dummyColumnExpression2);
        self::assertEquals($dummyColumnExpression1, $a->getColumnExpression());
        self::assertEquals($dummyColumnExpression2, $b->getColumnExpression());
        self::assertEquals('columnName1', $a->getAlias());
        self::assertEquals(null, $b->getAlias());
        $a->setAlias('changedColumnName1');
        self::assertEquals('changedColumnName1', $a->getAlias());
    }

}
