<?php
/*
 * Copyright 2020 Marc Brünisholz
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

namespace ch\_4thewin\SqlSelectModels;


use PHPUnit\Framework\TestCase;

class SelectTest extends TestCase
{
    public function test()
    {
        $a = new Select(new Table('tableName1', 'id', 'string'));

        self::assertEquals(new Table('tableName1', 'id', 'string'), $a->getFromClause());
        self::assertNull($a->getWhereCondition());
        self::assertEmpty($a->getJoins());
        self::assertEmpty($a->getSelectedColumns());
        self::assertFalse($a->isDistinct());

        $whereCondition = new TestParameterizedSql();
        $a->setWhereCondition($whereCondition);
        self::assertEquals($whereCondition, $a->getWhereCondition());
        $dummyCondition1 = new TestParameterizedSql();
        $a->addJoin(
            new Join(
                new Table('owningTable', 'id', 'string'),
                'foreignKey',
                new Table('joinedTableName1', 'id', 'string','jTN1'),
                true,
                null
            )
        );
        $dummyCondition2 = new TestParameterizedSql();

        $a->addJoin(
            new Join(
                new Table('owningTable', 'id', 'string'),
                'foreignKey',
                new Table('joinedTableName2', 'id','string', 'jTN2'),
                true,
                $dummyCondition2
            )
        );
        self::assertEquals('joinedTableName1', $a->getJoins()[0]->getInverseTable()->getName());
        self::assertEquals('jTN1', $a->getJoins()[0]->getInverseTable()->getAlias());
        self::assertNull(
            $a->getJoins()[0]->getCondition()
        );
        self::assertEquals('joinedTableName2', $a->getJoins()[1]->getInverseTable()->getName());
        self::assertEquals('jTN2', $a->getJoins()[1]->getInverseTable()->getAlias());
        self::assertEquals(
            $dummyCondition2,
            $a->getJoins()[1]->getCondition()
        );
        $dummyCondition3 = new TestParameterizedSql();

        $joins = [
            new Join(
                new Table('owningTable', 'id', 'string'),
                'foreignKey',
                new Table('joinedTableName3', 'id',  'string', 'jTN3'),
                true,
                $dummyCondition3
            )
        ];
        $a->setJoins($joins);
        self::assertCount(1, $a->getJoins());
        self::assertEquals('joinedTableName3', $a->getJoins()[0]->getInverseTable()->getName());
        self::assertEquals('jTN3', $a->getJoins()[0]->getInverseTable()->getAlias());
        self::assertEquals(
            $dummyCondition3,
            $a->getJoins()[0]->getCondition()
        );
        $dummyColumnExpression1 = new TestParameterizedSql();
        $dummyColumnExpression2 = new TestParameterizedSql();
        $a->selectColumn($dummyColumnExpression1, 'column1Name');
        self::assertEquals($dummyColumnExpression1, $a->getSelectedColumns()[0]->getColumnExpression());
        self::assertEquals('column1Name', $a->getSelectedColumns()[0]->getAlias());
        $a->setSelectedColumns([new SelectedColumn($dummyColumnExpression2, 'column2Name')]);
        self::assertEquals($dummyColumnExpression2, $a->getSelectedColumns()[0]->getColumnExpression());
        self::assertEquals('column2Name', $a->getSelectedColumns()[0]->getAlias());
        $a->setIsDistinct(true);
        self::assertTrue($a->isDistinct());
    }

}
