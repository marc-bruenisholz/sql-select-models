<?php

namespace ch\_4thewin\SqlSelectModels;

use PHPUnit\Framework\TestCase;

class CTETest extends TestCase
{

    public function testCTE() {
        $select = new Select(new Table('cteName', 'id', 'string'));
        $select->addCTE('cteName', new Select(new Table('tableName', 'id', 'string')));

        self::assertCount(1, $select->getAllCTEs());
        self::assertEquals(new Select(new Table('tableName', 'id', 'string')), $select->getAllCTEs()['cteName']);
        self::assertEquals(new Select(new Table('tableName', 'id', 'string')), $select->getCTE('cteName'));
    }
}